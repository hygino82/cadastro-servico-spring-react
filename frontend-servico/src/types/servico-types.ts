export type Servico = {
  nomeCliente: string;
  dataInicio: string;
  dataTermino: string;
  descricao: string;
  valorServico: number;
  valorPago: number;
  dataPagamento: string;
  status: string;
  id:number;
};

export type ListaServico={
  data :Servico[];
  status:number;
}