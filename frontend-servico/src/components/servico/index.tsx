import axios from "axios";
import { useEffect, useState } from "react";
import { Servico } from "../../types/servico-types";
import { BASE_URL } from "../../utils/base-url";
import "./style.css";

export function ServicoForm() {
  const [servico, setServico] = useState<Servico>({
    nomeCliente: "",
    descricao: "",
    valorPago: 0.0,
    valorServico: 0.0,
    dataInicio: "",
    dataTermino: "",
    dataPagamento: "",
    status: "",
    id: 0,
  });

  function limparCampos() {
    setServico({
      nomeCliente: "",
      descricao: "",
      valorPago: 0.0,
      valorServico: 0.0,
      dataInicio: "",
      dataTermino: "",
      dataPagamento: "",
      status: "",
      id: 0,
    });
  }
  const [servicos, setServicos] = useState<Servico[]>([]);
  const [atualizar, setAtualizar] = useState<any>();

  useEffect(() => {
    buscarTodos();
  }, [atualizar]);

  function buscarTodos() {
    axios.get(`${BASE_URL}/servico`).then((result) => {
      const valores: Servico[] = result.data;
      setServicos(valores);
    });
  }

  function buscarCancelados() {
    axios.get(`${BASE_URL}/servico/cancelados`).then((result) => {
      const valores: Servico[] = result.data;
      setServicos(valores);
    });
  }
  function buscarPendentes() {
    axios.get(`${BASE_URL}/servico/pendentes`).then((result) => {
      const valores: Servico[] = result.data;
      setServicos(valores);
    });
  }

  function handleChange(event: any) {
    setServico({ ...servico, [event.target.name]: event.target.value });
  }

  function handleSubmit(event: any) {
    event.preventDefault();
    if (servico.id) {
      //console.log(`Modificando objeto com o Id: ${servico.id}`);
      axios.put(`${BASE_URL}/servico/${servico.id}`, servico).then((result) => {
        setAtualizar(result);
      });
    } else {
      //console.log(`Registro criado`);
      axios.post(`${BASE_URL}/servico`, servico).then((result) => {
        setAtualizar(result);
      });
    }
    limparCampos();
  }

  function handleCancelar(id: number) {
    axios
      .patch(`${BASE_URL}/servico/cancelar/${id}`)
      .then((result) => setAtualizar(result));
  }

  function handleDeletar(id: number) {
    axios
      .delete(`${BASE_URL}/servico/${id}`)
      .then((result) => setAtualizar(result));
  }

  return (
    <div className="container">
      <h1>Cadastro de Serviços</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <div className="col-6">
            <label className="form-label">Nome do Cliente</label>
            <input
              type="text"
              value={servico.nomeCliente || ""}
              name="nomeCliente"
              className="form-control"
              onChange={handleChange}
            />
          </div>
          <div className="col-6">
            <label className="form-label">Data de Início</label>
            <input
              type="date"
              value={servico.dataInicio || ""}
              name="dataInicio"
              className="form-control"
              onChange={handleChange}
            />
          </div>
          <div className="col-6">
            <label className="form-label">Data de Término</label>
            <input
              type="date"
              value={servico.dataTermino || ""}
              name="dataTermino"
              className="form-control"
              onChange={handleChange}
            />
          </div>
          <div className="col-6">
            <label className="form-label">Descrição do Serviço</label>
            <input
              type="text"
              value={servico.descricao || ""}
              name="descricao"
              className="form-control"
              onChange={handleChange}
            />
          </div>
          <div className="col-6">
            <label className="form-label">Valor do Pago</label>
            <input
              type="number"
              value={servico.valorPago || ""}
              name="valorPago"
              className="form-control"
              onChange={handleChange}
            />
          </div>
          <div className="col-6">
            <label className="form-label">Valor do Serviço</label>
            <input
              type="number"
              value={servico.valorServico || ""}
              name="valorServico"
              className="form-control"
              onChange={handleChange}
            />
          </div>
          <div className="col-6">
            <label className="form-label">Data de Pagamento</label>
            <input
              type="date"
              value={servico.dataPagamento || ""}
              name="dataPagamento"
              className="form-control"
              onChange={handleChange}
            />
          </div>
        </div>
        <br />
        <input type="submit" value="Cadastrar" className="btn btn-success" />
      </form>
      <hr />
      <hr />

      <button
        onClick={() => buscarTodos()}
        type="button"
        className="btn btn-primary"
      >
        Listar Todos
      </button>
      <button
        onClick={() => buscarPendentes()}
        type="button"
        className="btn btn-danger"
      >
        Pagamento Pendentes
      </button>
      <button
        onClick={() => buscarCancelados()}
        type="button"
        className="btn btn-warning"
      >
        Serviços Cancelados
      </button>

      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">Descrição</th>
            <th scope="col">Valor Serviço</th>
            <th scope="col">Status</th>
            <th scope="col">Opções</th>
          </tr>
        </thead>
        <tbody>
          {servicos.map((servico) => {
            return (
              <tr key={servico.id}>
                <th scope="row">{servico.nomeCliente}</th>
                <td>{servico.descricao}</td>
                <td>{servico.valorServico}</td>
                <td>{servico.status}</td>
                <td>
                  {servico.status != "cancelado" && (
                    <button
                      onClick={() => setServico(servico)}
                      type="submit"
                      className="btn btn-primary"
                    >
                      Alterar
                    </button>
                  )}
                  &nbsp;&nbsp;
                  {servico.status != "cancelado" && (
                    <button
                      type="submit"
                      onClick={() => handleDeletar(servico.id)}
                      className="btn btn-danger"
                    >
                      Excluir
                    </button>
                  )}
                  &nbsp;&nbsp;
                  <button
                    type="submit"
                    className="btn btn-warning"
                    onClick={() => handleCancelar(servico.id)}
                  >
                    Cancelar
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
