import axios from "axios";
import { useState } from "react";
import { Servico } from "../../types/servico-types";
import { BASE_URL } from "./../../utils/base-url";

type Props = {
  servicos: Servico[];
};

const [servico, setServico] = useState<Servico>();

export function TabelaServico({ servicos }: Props, event: any) {
  function handleDelete(id: number) {
    event.preventDefault();
    axios
      .delete(`${BASE_URL}/servico/${id}`)
      .then(() => console.log(`Serviço com o Id: ${id} removido com sucesso`));
  }

  return (
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">Descrição</th>
            <th scope="col">Valor Serviço</th>
            <th scope="col">Status</th>
            <th scope="col">Opções</th>
          </tr>
        </thead>
        <tbody>
          {servicos.map((servico) => {
            return (
              <tr key={servico.id}>
                <th scope="row">{servico.nomeCliente}</th>
                <td>{servico.descricao}</td>
                <td>{servico.valorServico}</td>
                <td>{servico.status}</td>
                <td>
                  <button
                    onClick={() => setServico(servico)}
                    type="submit"
                    className="btn btn-primary"
                  >
                    Alterar
                  </button>
                  &nbsp;&nbsp;
                  <button type="submit" className="btn btn-danger">
                    Excluir
                  </button>
                  &nbsp;&nbsp;
                  <button type="submit" className="btn btn-warning">
                    Cancelar
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
