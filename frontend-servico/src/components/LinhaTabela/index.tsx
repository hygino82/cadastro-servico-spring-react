import { Servico } from "../../types/servico-types.ts";
type Props = {
  servico: Servico;
};

export function LinhaTabela({ servico }: Props) {
  return (
    <tr key={servico.id}>
      <th scope="row">{servico.nomeCliente}</th>
      <td>{servico.descricao}</td>
      <td>{servico.valorServico}</td>
      <td>{servico.status}</td>
      <td>opçoes</td>
    </tr>
  );
}
