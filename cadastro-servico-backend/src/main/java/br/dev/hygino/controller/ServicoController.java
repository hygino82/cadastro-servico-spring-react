package br.dev.hygino.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.dev.hygino.entity.Servico;
import br.dev.hygino.service.ServicoService;

@RestController
@RequestMapping("/servico")
@CrossOrigin("*")
public class ServicoController {
    private final ServicoService servicoService;

    public ServicoController(ServicoService servicoService) {
        this.servicoService = servicoService;
    }

    @GetMapping
    public ResponseEntity<List<Servico>> buscarTodos() {
        var res = servicoService.buscarTodos();
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    @PostMapping
    public ResponseEntity<Servico> inserir(@RequestBody Servico servico) {
        return ResponseEntity.status(HttpStatus.CREATED).body(servicoService.inserir(servico));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Servico> atualizar(@PathVariable("id") Long id, @RequestBody Servico servico) {
        Servico res = servicoService.atualizar(id, servico);
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        servicoService.remover(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/cancelados")
    public ResponseEntity<List<Servico>> buscarCancelados() {
        var res = servicoService.listarCancelados();
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    @GetMapping("/pendentes")
    public ResponseEntity<List<Servico>> buscarPendentes() {
        var res = servicoService.buscarPendentes();
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    @PatchMapping("/cancelar/{id}")
    public ResponseEntity<Servico> cancelaServico(@PathVariable("id") Long id) {
        Servico res = servicoService.cancelaServico(id);
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }
}
