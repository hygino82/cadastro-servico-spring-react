package br.dev.hygino.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.dev.hygino.entity.Servico;

public interface ServicoRepository extends JpaRepository<Servico, Long> {

    @Query("SELECT obj FROM Servico obj")
    List<Servico> buscarTodos();

    // @Query("SELECT obj FROM Servico obj WHERE UPPER(obj.status) = 'PENDENTE'")
    @Query("SELECT obj FROM Servico obj WHERE (obj.valorPago is null OR obj.valorPago = 0.0) AND obj.status <> 'cancelado'")
    List<Servico> listarPendentes();

    @Query("SELECT obj FROM Servico obj WHERE UPPER(obj.status) = 'CANCELADO'")
    List<Servico> listarCancelados();

    @Query("UPDATE Servico obj SET obj.status ='cancelado' WHERE id = :id")
    void cancelarServico(long id);

}
