package br.dev.hygino.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.dev.hygino.entity.Servico;
import br.dev.hygino.repository.ServicoRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class ServicoService {
    private final ServicoRepository servicoRepository;

    public ServicoService(ServicoRepository servicoRepository) {
        this.servicoRepository = servicoRepository;
    }

    @Transactional(readOnly = true)
    public List<Servico> buscarTodos() {
        return servicoRepository.buscarTodos();
    }

    @Transactional
    public Servico inserir(Servico servico) {
        if (servico.getValorPago() == null || servico.getValorPago() == 0.0 || servico.getDataPagamento() == null) {
            servico.setStatus("pendente");
        } else {
            servico.setStatus("realizado");
        }
        return servicoRepository.saveAndFlush(servico);
    }

    @Transactional
    public Servico atualizar(Long id, Servico servico) {
        Servico res = servicoRepository.getReferenceById(id);
        try {
            if (servico.getValorPago() != null && servico.getValorPago() > 0.0 && servico.getDataPagamento() == null) {
                servico.setStatus("realizado");
            }
            res = copiarValores(servico, res);
            res = servicoRepository.saveAndFlush(res);
            return res;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public void remover(Long id) {
        servicoRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<Servico> listarCancelados() {
        return servicoRepository.listarCancelados();
    }

    @Transactional(readOnly = true)
    public List<Servico> buscarPendentes() {
        return servicoRepository.listarPendentes();
    }

    private Servico copiarValores(Servico origem, Servico destino) {
        destino.setDataInicio(origem.getDataInicio());
        destino.setDataPagamento(origem.getDataPagamento());
        destino.setDataTermino(origem.getDataTermino());
        destino.setValorPago(origem.getValorPago());
        destino.setDescricao(origem.getDescricao());
        destino.setNomeCliente(origem.getNomeCliente());
        destino.setValorServico(origem.getValorServico());
        destino.setStatus(origem.getStatus());

        return destino;
    }

    @Transactional
    public Servico cancelaServico(Long id) {
        try {
            Servico servico = servicoRepository.getReferenceById(id);
            servico.setStatus("cancelado");
            servico = servicoRepository.saveAndFlush(servico);
            return servico;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }
}
