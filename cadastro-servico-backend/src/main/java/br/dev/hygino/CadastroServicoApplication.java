package br.dev.hygino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastroServicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastroServicoApplication.class, args);
	}

}
