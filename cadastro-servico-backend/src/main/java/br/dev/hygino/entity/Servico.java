package br.dev.hygino.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "servico")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Servico implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotEmpty
    private String nomeCliente;

    @Temporal(TemporalType.DATE)
    private Date dataInicio = new Date();

    @Temporal(TemporalType.DATE)
    private Date dataTermino;

    @NotEmpty
    private String descricao;

    @NotNull
    private Double valorServico;

    private Double valorPago = 0.0;

    @Temporal(TemporalType.DATE)
    private Date dataPagamento;

    private String status;// pendente, realizado, cancelado
}
